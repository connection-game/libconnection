"use strict";

const From = Object.freeze({ server: "server", player: "player" /*probably temporary*/ });
const To = Object.freeze({ server: "server", broadcast: "broadcast", player: "player" });
const Type = Object.freeze({ 
    moveEvent: "moveEvent", 
    statusEvent: "statusEvent",
    playerDisconnected: "playerDisconnected",
    playerConnected: "playerConnected",
    playerID: "playerID",
    error: "error",
    roomInfo: "roomInfo"
});
const Errors = Object.freeze({
    unknownTo: "unknownTo",
    parseError: "parseError",
    unknownError: "unknownError",
});

class Envelope {
    constructor(to, type, data={}, from=undefined) {
        this.to = to;
        this.type = type;
        this.data = data;
        this.from = from;
    }
}

class Error extends Envelope {
    constructor(to, message) {
        super(to, Type.error, { message: message }, From.server);
    }
}

class PlayerDisconnected extends Envelope {
    constructor(uuid) {
        super(To.broadcast, Type.playerDisconnected, { uuid: uuid }, From.server);
    }
}

class PlayerConnected extends Envelope {
    constructor(uuid) {
        super(To.broadcast, Type.playerConnected, { id: uuid }, From.server);
    }
}

class PlayerID extends Envelope {
    constructor(uuid) {
        super(uuid, Type.playerID, undefined, From.server);
    }
}

class RoomInfo extends Envelope {
    constructor(to, numOfPlayers, roomId) {
        super(to, Type.roomInfo, { numOfPlayers: numOfPlayers, roomId: roomId }, From.server);
    }
}

const Protocol = {
    Envelope: Envelope,
    From: From,
    To: To,
    Type: Type,
    PlayerDisconnected: PlayerDisconnected,
    PlayerConnected: PlayerConnected,
    PlayerID: PlayerID,
    RoomInfo: RoomInfo,
    Error: Error,
    Errors: Errors,
};


if (require.main !== module) {
    module.exports = Protocol;
}
